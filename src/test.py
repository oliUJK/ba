import cv2 as cv

from system_analyzer import get_cam_names, VideoDevice

from dataclasses import dataclass
import typing
import platform
from pathlib import Path
import time

directory = Path(__file__).parent


@dataclass
class ImageResolution:
    x: int
    y: int


def configure_stream(cap: cv.VideoCapture, resolution: ImageResolution) -> dict:
    actual_resolution = ImageResolution(int(cap.get(cv.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv.CAP_PROP_FRAME_HEIGHT)))
    print(actual_resolution)

    # Setting resolution
    print(f'Set-Resolution: {resolution}...')
    cap.set(cv.CAP_PROP_FRAME_WIDTH, resolution.x)
    cap.set(cv.CAP_PROP_FRAME_HEIGHT, resolution.y)
    actual_resolution = ImageResolution(int(cap.get(cv.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv.CAP_PROP_FRAME_HEIGHT)))

    if actual_resolution == resolution:
        print("success!")
    else:
        error_str = f"Target resolution could not be set. Actual resolution is: {actual_resolution}."
        print(error_str)
        raise RuntimeError(error_str)

    return {"resolution": actual_resolution}


def setup_camera(resolution: ImageResolution, stream_idx=None) -> typing.Tuple[cv.VideoCapture, ImageResolution]:
    streams = get_cam_names(verbose=True)

    if stream_idx is None:
        stream_idx = int(input("\nEnter the stream index: "))

    selection: VideoDevice = streams[stream_idx]

    print(selection)

    if platform.system() == "Windows":
        cap = cv.VideoCapture(selection.path, cv.CAP_DSHOW)
    else:
        cap = cv.VideoCapture(selection.path)

    if cap.read() == None:
        raise ConnectionError("Could not open camera stream.")

    cam_name = selection.serial
    results = configure_stream(cap, resolution)
    actual_resolution = results["resolution"]

    # -------------------------------------------------------------------------------------------------------------------------- load calibration file
    # cam_type_calib_filename = "Data\\cam_type_calibration_.json"
    cam_type_calib_filename = "Data/20230130-chessboard_calib_results.json"

    return (cap, actual_resolution)

#resolution = ImageResolution(3840, 2160)
resolution = ImageResolution(1280, 720)
#resolution = ImageResolution(1920, 1080)
#resolution = ImageResolution(2560, 1472)

#cap, resolution = setup_camera(resolution, camera)
cap, resolution = setup_camera(resolution, stream_idx=0)
print(resolution)
if not cap.isOpened():
    print("Cannot open camera")
    exit()
print(1)

imgname = "curFrame"
ret, img = cap.read()
cv.imshow(imgname, img)
key = 0

while cv.getWindowProperty(imgname, cv.WND_PROP_VISIBLE) > 0:
    ret, img = cap.read()

    # 32 = Leertaste
    if key == 32:
        output_dir = directory.parent.joinpath("pic")
        str_ms = f"{int((time.time() % 1.0)*1000):03d}"
        time_str = time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime())
        time_str += f".{str_ms}"
        filename = output_dir.joinpath(f"Bild_{time_str}.jpg")
        cv.imwrite(str(filename), img)
        print(f"Saved to {time_str}")

    # cv.imshow('window', img)
    cv.rectangle(img, (500, 210), (720, 430), (0, 255, 0), 2)
    cv.imshow(imgname, img)

    key = cv.waitKey(10)
    # 27 = ESC
    if key == 27:
        break


 #   if cv.waitKey(1) & 0xFF == ord('q'):
 #       break
cap.release()
cv.destroyAllWindows()
