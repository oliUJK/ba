"""
unit for analyzing the system for windows and Linux
-> not platform independent!
"""

import platform
import typing
from dataclasses import dataclass

if platform.system() == "Windows":
    from pygrabber.dshow_graph import FilterGraph
elif platform.system() == "Linux":
    import pyudev
else:
    raise NotImplementedError(f"Only Windows and Linux are supported currently. Active platform is {platform.system()}")

from typing import Union, List, Tuple
import cv2 as cv

@dataclass
class VideoDevice:
    path: str
    vendor_id: int
    product_id: int
    vendor: str
    product: str
    serial: str


def get_cam_names_pygrabber(verbose: bool = False) -> List[str]:
    """
    https://github.com/bunkahle/pygrabber -> using DirectShow!
     -> returns all video devices of the DShow filter graph
    """
    graph = FilterGraph()
    filter_graph = graph.filter_graph.value   # TODO: How to access the graph structure?

    # ------------------------------------------------------------- force a list of strings
    device_list: List[VideoDevice] = []
    for i, device in enumerate(graph.get_input_devices()):
        dev = VideoDevice(path=i, serial=str(device), vendor_id=None, product_id=None, vendor="", product="")
        device_list.append(dev)

    if verbose:
        print('With pygrabber and DSHOW detected camera streams:')
        for idx, device in enumerate(device_list):
            print(f"\t{idx} - {device.serial} @ {device.path}")

    return device_list


def get_cam_names_pyudev(verbose: bool = False) -> VideoDevice:

    streams: typing.List[VideoDevice] = list()
    context = pyudev.Context()
    for device in context.list_devices(subsystem='video4linux'):
        dpath = device.get("DEVNAME", "unknown device.path")
        vendor = device.get("ID_VENDOR", "vendor not found")
        vid = device.get("ID_VENDOR_ID", "vendorId not found")
        product = device.get("ID_MODEL", "product not found")
        pid = device.get("ID_MODEL_ID", "productId not found")
        serial = device.get("ID_SERIAL", "serial not found")
        caps = device.get("ID_V4L_CAPABILITIES", "capabilities not found")

        # _logger.debug(f"Capabilities: {caps}")
        if "capture" in caps:
            dev = VideoDevice(dpath, vid, pid, vendor, product, serial)
            streams.append(dev)
            # _logger.debug(dev)

    if len(streams) < 1:
        raise RuntimeError("Could not find valid devices.")

    if verbose:
        print('With udev detected capturing camera streams:')
        for idx, device in enumerate(streams):
            print(f"\t{idx} - {device.serial} @ {device.path}")

    return streams


def get_cam_names(verbose: bool = False) -> typing.List[typing.Tuple[str, typing.Any]]:
    if platform.system() == "Windows":
        return get_cam_names_pygrabber(verbose=verbose)
    else:
        return get_cam_names_pyudev(verbose=verbose)


# ====================================================================================================================================================
if __name__ == "__main__":
    get_cam_names(True)
